-- Fake view aus sich selbst
-- Version mpg ohne mpg_chem
CREATE OR REPLACE VIEW view_gefahr AS
 SELECT
  '1' AS autoID,
  '1' AS tabID,
  'Fake Eintrag wenn keine chemDB' AS substanz,
  '-' AS kategorie,
  'mpg' AS gruppe,
  'initial' AS bearbeiter,
  '2016-03-02 11:34:18' AS zeitstempel
;

-- Version mpg mit mpg_chem
-- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren - siehe mpi-dcts
CREATE OR REPLACE VIEW view_gefahr AS
 SELECT
  mpgT1.autoID,
  mpgT2.tabID,
  mpgT2.substanz,
  mpgT1.kategorie,
  'mpg' AS gruppe,
  mpgT1.bearbeiter,
  mpgT1.zeitstempel
 FROM
  mpidb_mpg_chem.mpi_gefahr AS mpgT1,
  mpidb_mpg_chem.mpi_chemstoff AS mpgT2
 WHERE
  mpgT1.chemID = mpgT2.tabID
;


-- Version mpi-dcts
CREATE OR REPLACE VIEW view_gefahr AS
 SELECT
  bioT1.autoID,
  bioT2.tabID,
  bioT2.substanz,
  bioT1.kategorie,
  'bio' AS gruppe,
  bioT1.bearbeiter,
  bioT1.zeitstempel
 FROM
  mpidb_bio_chem.mpi_gefahr AS bioT1,
  mpidb_bio_chem.mpi_chemstoff AS bioT2
 WHERE
  bioT1.chemID = bioT2.tabID
UNION ALL
 SELECT
  pcgT1.autoID,
  pcgT2.tabID,
  pcgT2.substanz,
  pcgT1.kategorie,
  'pcg' AS gruppe,
  pcgT1.bearbeiter,
  pcgT1.zeitstempel
 FROM
  mpidb_pcg_chem.mpi_gefahr AS pcgT1,
  mpidb_pcg_chem.mpi_chemstoff AS pcgT2
 WHERE
  pcgT1.chemID = pcgT2.tabID
UNION ALL
 SELECT
  bpeT1.autoID,
  bpeT2.tabID,
  bpeT2.substanz,
  bpeT1.kategorie,
  'bpe' AS gruppe,
  bpeT1.bearbeiter,
  bpeT1.zeitstempel
 FROM
  mpidb_bpe_chem.mpi_gefahr AS bpeT1,
  mpidb_bpe_chem.mpi_chemstoff AS bpeT2
 WHERE
  bpeT1.chemID = bpeT2.tabID
UNION ALL
 SELECT
  pseT1.autoID,
  pseT2.tabID,
  pseT2.substanz,
  pseT1.kategorie,
  'pse' AS gruppe,
  pseT1.bearbeiter,
  pseT1.zeitstempel
 FROM
  mpidb_pse_chem.mpi_gefahr AS pseT1,
  mpidb_pse_chem.mpi_chemstoff AS pseT2
 WHERE
  pseT1.chemID = pseT2.tabID
;


