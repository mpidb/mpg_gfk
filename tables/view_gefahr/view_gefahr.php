<?php

class tables_view_gefahr { 

  // check login & editieren nicht erlaubt
  function getPermissions($record) {
    $auth =& Dataface_AuthenticationTool::getInstance();
    $user =& $auth->getLoggedInUser();
    if ( !isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
    return Dataface_PermissionsTool::getRolePermissions('READ ONLY');
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    return $table;
  }

  // link nach Substanz
  function substanz__renderCell( &$record ) {
    $subst = $record->strval('substanz');
    $tabID = $record->strval('tabID');
    $subdb = $record->strval('gruppe').'_chem';
    $table = 'mpi_chemstoff';
    return '<div style="white-space:nowrap"><a href="/'.$subdb.'/index.php?-table='.$table.'&-action=browse&tabID='.$tabID.'">'.$subst.'</a></div>';
  }

  function valuelist__cmr() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__anweisung() {
    return array(0=>'nein', 1=>'ja');
  }

  // Formatiere Zeitstempel auf Deutsch
  function zeitstempel__display(&$record)  {
    if ($record->strval('zeitstempel') == NULL) return;
    return date('d.m.Y', strtotime($record->strval('zeitstempel')));
  }

}

?>
