-- version alone
-- fake auf sich selbst, also ohne db_chem's
CREATE OR REPLACE VIEW view_chemAll AS
 SELECT
  '1' AS tabID,
  'Fake Eintrag wenn keine chemDB' AS substanz,
  '-' AS reinheit,
  '-' AS lgk,
  '-' AS cas,
  '-' AS einheit,
  '0' AS bFileID,
  '-' AS bFilename,
  '0' AS sFileID,
  '-' AS sFilename,
  'mpg' AS gruppe,
  'initial' AS bearbeiter,
  '2016-05-03 11:34:18' AS zeitstempel
;


-- version mpg mit mpg_chem
-- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
-- siehe Beispiel mpi-dcts (4db's) in ../tables/view_chemAll/view_chemAll.sql 
CREATE OR REPLACE VIEW view_chemAll AS
 SELECT
  mpg.tabID AS tabID,
  mpg.substanz AS substanz,
  mpg.reinheit AS reinheit,
  mpg.lgk AS lgk,
  mpg.cas AS cas,
  mpg.einheit AS einheit,
  LPAD(mpg.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.bFileID = ablageID) AS bFilename,
  LPAD(mpg.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_mpg_chem.mpi_ablage WHERE mpg.sFileID = ablageID) AS sFilename,
  'mpg' AS gruppe,
  mpg.bearbeiter AS bearbeiter,
  mpg.zeitstempel AS zeitstempel
 FROM mpidb_mpg_chem.mpi_chemstoff AS mpg
;


-- version mpi-dcts
-- verknuepft mit 4 db's
CREATE OR REPLACE VIEW view_chemAll AS
 SELECT
  bio.tabID AS tabID,
  bio.substanz AS substanz,
  bio.reinheit AS reinheit,
  bio.lgk AS lgk,
  bio.cas AS cas,
  bio.einheit AS einheit,
  LPAD(bio.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_bio_chem.mpi_ablage WHERE bio.bFileID = ablageID) AS bFilename,
  LPAD(bio.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_bio_chem.mpi_ablage WHERE bio.sFileID = ablageID) AS sFilename,
  'bio' AS gruppe,
  bio.bearbeiter AS bearbeiter,
  bio.zeitstempel AS zeitstempel
 FROM mpidb_bio_chem.mpi_chemstoff AS bio
UNION ALL
 SELECT
  bpe.tabID AS tabID,
  bpe.substanz AS substanz,
  bpe.reinheit AS reinheit,
  bpe.lgk AS lgk,
  bpe.cas AS cas,
  bpe.einheit AS einheit,
  LPAD(bpe.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_bpe_chem.mpi_ablage WHERE bpe.bFileID = ablageID) AS bFilename,
  LPAD(bpe.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_bpe_chem.mpi_ablage WHERE bpe.sFileID = ablageID) AS sFilename,
  'bpe' AS gruppe,
  bpe.bearbeiter AS bearbeiter,
  bpe.zeitstempel AS zeitstempel
 FROM
  mpidb_bpe_chem.mpi_chemstoff AS bpe 
UNION ALL
 SELECT
  pcg.tabID AS tabID,
  pcg.substanz AS substanz,
  pcg.reinheit AS reinheit,
  pcg.lgk AS lgk,
  pcg.cas AS cas,
  pcg.einheit AS einheit,
  LPAD(pcg.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_pcg_chem.mpi_ablage WHERE pcg.bFileID = ablageID) AS bFilename,
  LPAD(pcg.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_pcg_chem.mpi_ablage WHERE pcg.sFileID = ablageID) AS sFilename,
  'pcg' AS gruppe,
  pcg.bearbeiter AS bearbeiter,
  pcg.zeitstempel AS zeitstempel
 FROM
  mpidb_pcg_chem.mpi_chemstoff AS pcg 
UNION ALL
 SELECT
  pse.tabID AS tabID,
  pse.substanz AS substanz,
  pse.reinheit AS reinheit,
  pse.lgk AS lgk,
  pse.cas AS cas,
  pse.einheit AS einheit,
  LPAD(pse.bFileID, 6, '0') AS bFileID,
  (SELECT file_filename FROM mpidb_pse_chem.mpi_ablage WHERE pse.bFileID = ablageID) AS bFilename,
  LPAD(pse.sFileID, 6, '0') AS sFileID,
  (SELECT file_filename FROM mpidb_pse_chem.mpi_ablage WHERE pse.sFileID = ablageID) AS sFilename,
  'pse' AS gruppe,
  pse.bearbeiter AS bearbeiter,
  pse.zeitstempel AS zeitstempel
 FROM
  mpidb_pse_chem.mpi_chemstoff AS pse
;
