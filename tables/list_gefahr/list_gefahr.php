<?php

class tables_list_gefahr { 

  function block__before_fineprint() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $mailto = $app->_conf['_own']['mailto'];
    $mname  = $app->_conf['_own']['mailname'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep == $db_ver )
      echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> = <span style="color: green">DB: '.$db_ver.'</span> &copy;<a href="mailto:'.$mailto.'">'.$mname.'</a>';
  }

  function block__before_body() {
    $app    = Dataface_Application::getInstance();
    $fs_ver = $app->_conf['_own']['version'];
    $sql    = "SELECT MAX(version) FROM dataface__version";
    list($db_ver) = xf_db_fetch_row(xf_db_query($sql, df_db()));
    $db_ver = str_pad($db_ver, 4, 0, STR_PAD_LEFT);
    $fs_rep = str_replace('.','',$fs_ver);
    if ( $fs_rep != $db_ver ) {
      if ( $fs_rep > $db_ver ) {
        echo 'Version <span style="color: green">FS: '.$fs_ver.'</span> &ne; <span style="color: red">DB: '.$db_ver.'</span>';
      } else {
        echo 'Version <span style="color: red">FS: '.$fs_ver.'</span> &ne; <span style="color: green">DB: '.$db_ver.'</span>';
      }
    }
  }

  // setze indiv. Farbe in listenansicht fuer tabelle
  function css__tableRowClass( &$record ) {
    $query = Dataface_Application::getInstance()->getQuery();
    $table = $query['-table'].' ';
    // return $table.'normal'; //mit color
    if ($record->val('anweisung') == '1') return $table.'lineY';
    return $table;
  }

  // Anzeige in liste aendern
  function lgk__renderCell(&$record) {
    return $record->val('lgk');
  }

  function valuelist__cmr() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__anweisung() {
    return array(0=>'nein', 1=>'ja');
  }

  function valuelist__substition() {
    return array(0=>'nein', 1=>'ja');
  }

}

?>
