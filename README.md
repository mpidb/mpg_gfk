## Einleitung ##

Die Datenbank Gefahrenklassen ist eine uebergeordnete Datenbank, in der fuer alle Chemiedatenbanken gemeinsam die Lagerklassen und Gefahrenmerkmale gefuehrt werden. Im Idealfall werden diese Tabellen von der verantwortlichen Person fuer Arbeitssicherheit gepflegt<br>
Die abgesetzten Chemiedatenbanken (bei uns vier) greifen per View auf diese Taebllen lesbar zurueck.<br>
Gleichzeitig hat diese DB lesbaren Zugriff auf alle Chemiestoffe zur moeglichen Wahrung der Arbeitssicherheit<br>
<br>
Inhalte:
* Gefahrenmerkmale
* Lagerklassen mit Priorisierung

## Projekt downloaden ##

**Evtl. vorh. DB und Filesystem vorher sichern**<br>
<br>
Entweder von common/db_export das tar-File herunterladen oder per Befehl
~~~bash
git clone https://gitlab.mpcdf.mpg.de/mpidb/mpg_gfk.git
~~~
in das Wurzelverzeichnis des Webserver klonen.

## Installation ##

siehe LIEMICH.txt in Folder install<br>
Dort liegen auch alle noch notwendigen SQL-Skripte fuer die DB-Erstellung und weitere Verknuepfungen.

## Screenshot ##

<a href="install/db_gfk.png" title="Datenbank Gefahrenklassen"><img src="install/db_gfk.png" align="left" height="100" width="100"></a>
<br><br><br><br>

## Warnhinweis ##

Die Benutzung der vorausgefuellten Tabellen fuer Gefahren- und Lagerklassen geschieht auf eigene Gefahr. Bitte pruefen sie unbedingt erst diese Tabellen bevor sie diese Datenbank aktivieren. Die Eintraege dienen nur als Vorlage und haben keinen Anspruch auf Vollstaendigkeit.

## Lizenzbedingungen ##

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.<br>
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.<br>  
See the GNU General Public License for more details.<br>
See also the file LICENSE.txt here
