-- views fuer db mpg_chem auf mpg_gfk tabellen

-- view auf list_lgk von db gfk - version mpg
CREATE OR REPLACE VIEW mpidb_mpg_chem.view_lgk AS
SELECT
 autoID,
 lgk,
 beschreibung,
 priority
FROM
 mpidb_mpg_gfk.list_lgk
;


-- view auf list_gefahr von db gfk - version mpg 
CREATE OR REPLACE VIEW mpidb_mpg_chem.view_gefahr AS
SELECT
 *
FROM
 mpidb_mpg_gfk.list_gefahr
;

