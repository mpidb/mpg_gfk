-- run: mysql -u root -p mpidb_mech_inv < updateDB_0500.sql
-- UPDATES immer von der niefrigsten bis zur hoechsten version ausfuehren
-- damit IF benutzt werden kann, wird eine prozedur erzeugt und am ende ausgefuehrt
-- 
-- views, funcs, procs nach moeglichkeit nur einmal in der max version ausfuehren

USE mpidb_mpg_gfk;
DROP PROCEDURE IF EXISTS proc_update;
DELIMITER $$
CREATE PROCEDURE proc_update()
proc_label: BEGIN

-- initial value
IF ( SELECT MAX(version) FROM dataface__version ) = '0' THEN
 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1000');
END IF;

-- mindest version vorhanden
IF ( SELECT MAX(version) FROM dataface__version ) < '1000' THEN
 LEAVE proc_label;
END IF;


IF ( SELECT MAX(version) FROM dataface__version ) < '1000' THEN

-- CHANGES V1.0.00 :
-- ****************
-- fs::rsync - mpg_lgk initial

 TRUNCATE dataface__version;
 INSERT INTO dataface__version (version) VALUES ('1000');
END IF;


-- CHANGES V1.0.01 - 2016-04-25 
-- ****************************
 -- fs::rsync           - asi policy view BA und SDB
 -- db::list_reiter     - add table view_ablageAll
 -- db::view_chemALL    - add BA und SDB anderer gruppen

 IF ( SELECT MAX(version) FROM dataface__version ) < '1001' THEN

  -- add table view_ablageAll
  INSERT IGNORE INTO list_reiter (reiter, kategorie, favorit, history, bedeutung) VALUES ('view_ablageAll', 'View', '1', '0', 'Betriebsanw. und Sicherh.-Datenbl. andere Gruppen');

  -- change view_chemAll mpg-version
  -- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
  -- siehe Beispiel mpi-dcts (4db's) in ../tables/view_chemAll/view_chemAll.sql 
  -- fake auf sich selbst, also ohne db_chem's
  CREATE OR REPLACE VIEW view_chemAll AS
   SELECT
    '1' AS tabID,
    'Fake Eintrag wenn keine chemDB' AS substanz,
    '-' AS reinheit,
    '-' AS lgk,
    '0' AS cmr,
    '-' AS cas,
    '-' AS einheit,
    '-' AS bFileID,
    '-' AS bFilename,
    '-' AS sFileID,
    '-' AS sFilename,
    'mpg' AS gruppe,
    'initial' AS bearbeiter,
    '2016-03-02 11:34:18' AS zeitstempel
  ;

  
  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1001');
 END IF;


-- CHANGES V1.0.02 - 2016-05-03 
-- ****************************
 -- fs::rsync           - asi policy view BA und SDB
 -- db::view_ablageALL  - alle duerfen BA und SDB anderer gruppen sehen

 IF ( SELECT MAX(version) FROM dataface__version ) < '1002' THEN

  -- add view_ablageAll mpg-version
  -- entsprechend erweitern/anpassen, wenn mehr chemie-db's existieren
  -- siehe Beispiel mpi-dcts (4db's) in ../tables/view_ablageAll/view_ablageAll.sql 
  -- fake auf sich selbst, also ohne db_chem's
  CREATE OR REPLACE VIEW view_ablageAll AS
   SELECT
    '000001' AS ablageID,
    'Betriebsanweisung' AS kategorie,
    'Fake Eintrag wenn keine chemDB' AS bezeichnung,
    'noName' AS filename,
    'mpg' AS gruppe,
    'initial' AS bearbeiter,
    '2016-04-22 11:34:18' AS zeitstempel
  ;
  

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1002');
 END IF;
  

-- CHANGES V1.0.03 - 2016-05-30 
-- ****************************
 -- fs::rsync           - add field cmr in list_gefahr
 -- db::list_gefahr     - add field cmr in list_gefahr
 -- db::view_chemALL    - del field cmr

 IF ( SELECT MAX(version) FROM dataface__version ) < '1003' THEN

  -- add field cmr
  ALTER TABLE  list_gefahr ADD cmr TINYINT(1) NULL DEFAULT '0' AFTER merkmal ;

  -- change view_chemAll mpg-version
  CREATE OR REPLACE VIEW view_chemAll AS
   SELECT
    '1' AS tabID,
    'Fake Eintrag wenn keine chemDB' AS substanz,
    '-' AS reinheit,
    '-' AS lgk,
    '-' AS cas,
    '-' AS einheit,
    '0' AS bFileID,
    '-' AS bFilename,
    '0' AS sFileID,
    '-' AS sFilename,
    'mpg' AS gruppe,
    'initial' AS bearbeiter,
    '2016-05-30 11:34:18' AS zeitstempel
  ;


  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1003');
 END IF;


-- CHANGES V1.0.04 - 2016-06-09
-- ****************************
 -- fs::conf.ini        - change comment # to ;
 -- fs::cronjob         - update to mysqli driver

 IF ( SELECT MAX(version) FROM dataface__version ) < '1004' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1004');
 END IF;


-- CHANGES V1.0.05 - 2016-06-13
-- ****************************
 -- fs::conf         - link nach master geloescht, Sortierung von fields.ini nach ApplicationDelegate.php
 -- fs::fields.ini   - sql order raus

 IF ( SELECT MAX(version) FROM dataface__version ) < '1005' THEN

  -- nur file version

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1005');
 END IF;


-- CHANGES V1.0.06 - 2016-06-27
-- ****************************
 -- fs::conf         - Sortierung in ApplicationDelegate.php
 -- db::list_gefahr  - add field kennzeichen

 IF ( SELECT MAX(version) FROM dataface__version ) < '1006' THEN

  -- db::list_gefahr  - add field kennzeichen
  ALTER TABLE list_gefahr ADD kennzeichen VARCHAR(50) NULL AFTER lgk ;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1006');
 END IF;


-- CHANGES V1.0.07 - 2016-11-21
-- ****************************
-- fs|db::list_gefahr    - add field substition

 IF ( SELECT MAX(version) FROM dataface__version ) < '1007' THEN

  -- list_gefahr
  ALTER TABLE list_gefahr CHANGE cmr cmr TINYINT(1) NOT NULL DEFAULT '0';
  ALTER TABLE list_gefahr ADD substition TINYINT(1) NOT NULL DEFAULT '0' AFTER anweisung;

  TRUNCATE dataface__version;
  INSERT INTO dataface__version (version) VALUES ('1007');

 END IF;


-- CHANGES V1.1.00 - 2018-08-14
-- ****************************
-- UPDATE: change Autorisierung von mpi_user nach sys_user mit Rollentabelle
-- db::list_reiter - Aenderung tabellen anpassen
-- fs::sys_user,list_rolle - Anpassung neue Benutzerverwaltung

IF ( SELECT MAX(version) FROM dataface__version ) < '1100' THEN

  -- create table list_role
    -- DROP TABLE IF EXISTS `list_role`;
    CREATE TABLE IF NOT EXISTS `list_role` (
      `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
      `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- insert default roles
    INSERT IGNORE INTO `list_role` (`rolID`, `role`, `description`) VALUES
    (000001, 'NO ACCESS', 'No_Access'),
    (000002, 'READ ONLY', 'view, list, calendar, view xml, show all, find, navigate'),
    (000003, 'EDIT', 'READ_ONLY and edit, new record, remove, import, translate, copy'),
    (000004, 'DELETE', 'EDIT and delete and delete found'),
    (000005, 'OWNER', 'DELETE except navigate, new, and delete found'),
    (000006, 'REVIEWER', 'READ_ONLY and edit and translate'),
    (000007, 'USER', 'READ_ONLY and add new related record'),
    (000008, 'ADMIN', 'DELETE and xml_view'),
    (000009, 'MANAGER', 'ADMIN and manage, manage_migrate, manage_build_index, and install');
    -- unique, pri
    ALTER TABLE `list_role` ADD PRIMARY KEY IF NOT EXISTS (`rolID`), ADD UNIQUE KEY IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `list_role` MODIFY `rolID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

    -- create table sys_user
    -- DROP TABLE IF EXISTS `sys_user`;
    CREATE TABLE IF NOT EXISTS `sys_user` (
     `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL,
     `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
     `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
     `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
     `bearbeiter` varchar(20) COLLATE utf8_unicode_ci NULL,
     `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    -- unique, pri
    ALTER TABLE `sys_user`
     ADD PRIMARY KEY IF NOT EXISTS (`logID`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `login` (`login`) USING BTREE,
     ADD UNIQUE KEY  IF NOT EXISTS `email` (`email`),
     ADD KEY         IF NOT EXISTS `role` (`role`);
    -- auto_increment
    ALTER TABLE `sys_user` MODIFY `logID` smallint(6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
    -- Constraint
    ALTER TABLE `sys_user` ADD CONSTRAINT `sysUser_listRole` FOREIGN KEY IF NOT EXISTS (`role`) REFERENCES `list_role` (`role`);

    -- mpg-version ohne externe DB (licman,inv,gfk,chem,user) - Auslieferzustand
    CREATE OR REPLACE VIEW view_user AS
     SELECT
      '000001' AS userID, 'mpg_local' AS login, 'MPG, Version (mpg_local)' AS sort
     ;

    -- create if not exist list_reiter
    CREATE TABLE IF NOT EXISTS `list_reiter` (
     `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
     `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     `favorit` tinyint(1) NOT NULL DEFAULT '0',
     `history` tinyint(1) NOT NULL DEFAULT '0',
     `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `reiter` (`reiter`),
     KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface' AUTO_INCREMENT=9 ;

    -- create if not exist list_katReiter
    CREATE TABLE IF NOT EXISTS `list_katReiter` (
     `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
     `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
     PRIMARY KEY (`autoID`),
     UNIQUE KEY `kategorie` (`kategorie`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;
    -- add entries in list_katReiter
    UPDATE list_katReiter SET kategorie = 'Autorisierung' WHERE kategorie = 'Authorisierung';
    INSERT IGNORE INTO `list_katReiter` (`kategorie`) VALUES ('Autorisierung');

    -- add entries in list_reiter
    INSERT IGNORE INTO `list_reiter` (`reiter`, `kategorie`, `favorit`, `history`, `bedeutung`) VALUES
     ('sys_user', 'Autorisierung', 1, 1, 'Autorisierung und Berechtigung Benutzer'),
     ('list_role', 'Autorisierung', 1, 0, 'Liste aller Berechtigungen (Rollen)');
    UPDATE `list_reiter` SET `bedeutung` = 'Auswahlliste fuer aktive und nicht abgelaufene Benutzer' WHERE `reiter` = 'view_user';

    -- copy inserts from old mpi_users
    INSERT IGNORE INTO sys_user (login, password, role, email, bearbeiter, zeitstempel) SELECT username, password, role, email, 'import', zeitstempel FROM mpi_users;

    -- del old table mpi_users (if all done and work)
    DROP TABLE IF EXISTS `mpi_users`;
    DROP TABLE IF EXISTS `mpi_users__history`;
    DELETE FROM `list_reiter` WHERE `reiter` = 'mpi_users';

 TRUNCATE dataface__version ;
 INSERT INTO dataface__version (version) VALUES ('1100') ;

END IF;


END;
$$
DELIMITER ;

-- execute updates
CALL proc_update();
