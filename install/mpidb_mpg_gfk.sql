
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mpidb_mpg_gfk` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `mpidb_mpg_gfk`;
DROP TABLE IF EXISTS `dataface__failed_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__failed_logins` (
  `attempt_id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `time_of_attempt` int(11) NOT NULL,
  PRIMARY KEY (`attempt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__failed_logins` WRITE;
/*!40000 ALTER TABLE `dataface__failed_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__failed_logins` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__modules` (
  `module_name` varchar(255) NOT NULL,
  `module_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`module_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__modules` WRITE;
/*!40000 ALTER TABLE `dataface__modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__modules` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__mtimes` (
  `name` varchar(255) NOT NULL,
  `mtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__preferences` (
  `pref_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `table` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `record_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pref_id`),
  KEY `username` (`username`),
  KEY `table` (`table`),
  KEY `record_id` (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__preferences` WRITE;
/*!40000 ALTER TABLE `dataface__preferences` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__preferences` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__record_mtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__record_mtimes` (
  `recordhash` varchar(32) NOT NULL,
  `recordid` varchar(255) NOT NULL,
  `mtime` int(11) NOT NULL,
  PRIMARY KEY (`recordhash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__record_mtimes` WRITE;
/*!40000 ALTER TABLE `dataface__record_mtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `dataface__record_mtimes` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `dataface__version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dataface__version` (
  `version` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `dataface__version` WRITE;
/*!40000 ALTER TABLE `dataface__version` DISABLE KEYS */;
INSERT INTO `dataface__version` VALUES (1100);
/*!40000 ALTER TABLE `dataface__version` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_gefahr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_gefahr` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `hsatz` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `merkmal` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `cmr` tinyint(1) NOT NULL DEFAULT '0',
  `anweisung` tinyint(1) NOT NULL DEFAULT '0',
  `substition` tinyint(1) NOT NULL DEFAULT '0',
  `lgk` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `kennzeichen` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`),
  KEY `lgk` (`lgk`),
  CONSTRAINT `list_gefahr_ibfk_1` FOREIGN KEY (`lgk`) REFERENCES `list_lgk` (`lgk`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_gefahr` WRITE;
/*!40000 ALTER TABLE `list_gefahr` DISABLE KEYS */;
INSERT INTO `list_gefahr` VALUES (000001,'Unst. Expl.','H200','Instabil, explosiv',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000002,'Expl. 1.1','H201','Explosiv, Gefahr der Massenexplosion.',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000003,'Expl. 1.2','H202','Explosiv; große Gefahr durch Splitter, Spreng- und Wurfstücke.',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000004,'Expl. 1.3','H203','Explosiv; Gefahr durch Feuer, Luftdruck oder Splitter, Spreng- und Wurfstücke.',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000005,'Expl. 1.4','H204','Gefahr durch Feuer oder Splitter, Spreng- und Wurfstücke.',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000006,'Expl. 1.5','H205','Gefahr der Massenexplosion bei Feuer.',0,1,0,'1','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000007,'Flam. Gas 1','H220','Extrem entzündbares Gas.',0,0,0,'2A',NULL);
INSERT INTO `list_gefahr` VALUES (000008,'Flam. Gas 2','H221','Entzündbares Gas.',0,0,0,'2A','GHS02; extrem entzündbar');
INSERT INTO `list_gefahr` VALUES (000009,'Flam. Aerosol 1','H222','Extrem entzündbares Aerosol.',0,0,0,'2B','GHS02; extrem entzündbar');
INSERT INTO `list_gefahr` VALUES (000010,'Flam. Aerosol 2','H223','Entzündbares Aerosol.',0,0,0,'2B','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000011,'Flam. Liq. 1','H224 ','Flüssigkeit und Dampf extrem entzündbar.',0,0,0,'3','GHS02; extrem entzündbar');
INSERT INTO `list_gefahr` VALUES (000012,'Flam. Liq. 2','H225','Flüssigkeit und Dampf leicht entzündbar.',0,0,0,'3','GHS02; leicht entzündbar');
INSERT INTO `list_gefahr` VALUES (000013,'Flam. Liq. 3','H226','Flüssigkeit und Dampf entzuendbar.',0,0,0,'3','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000014,'Flam. Sol. 1 or 2','H228','Entzündbarer Feststoff.',0,0,0,'4.1B','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000016,'Self-react. A','H240','Erwärmung kann Explosion verursachen.',0,1,0,'4.1A','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000017,'Org. Perox. A','H240','Erwärmung kann Explosion verursachen.',0,1,0,'4.1A','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000018,'Self-react. B','H241','Erwärmung kann Brand oder Explosion verursachen.',0,0,0,'4.1A','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000019,'Org. Perox. B','H241','Erwärmung kann Brand oder Explosion verursachen.',0,0,0,'4.1A','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000020,'Self-react. CDEF','H242','Erwärmung kann Brand verursachen.',0,0,0,'5.2','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000021,'Org. Perox. CDEF','H242','Erwärmung kann Brand verursachen.',0,0,0,'5.2','GHS02; entzündbar');
INSERT INTO `list_gefahr` VALUES (000024,'Pyr. Liq. 1','H250','Entzündet sich in Berührung mit Luft von selbst.',0,1,0,'4.2','GHS02; selbstentzündlich');
INSERT INTO `list_gefahr` VALUES (000025,'Pyr. Sol. 1','H250','Entzündet sich in Berührung mit Luft von selbst.',0,1,0,'4.2','GHS02; selbstentzündlich');
INSERT INTO `list_gefahr` VALUES (000026,'Self-heat.1','H251','Selbsterhitzungsfähig; kann in Brand geraten.',0,1,0,'4.2','GHS02; selbstentzündlich');
INSERT INTO `list_gefahr` VALUES (000027,'Self-heat. 2','H252','In großen Mengen selbsterhitzungsfähig; kann in Brand geraten.',0,1,0,'4.2','GHS02; selbstentzündlich');
INSERT INTO `list_gefahr` VALUES (000028,'Water-react. 1','H260','In Berührung mit Wasser entstehen entzündbare Gase, die sich spontan entzünden können.',0,0,0,'4.3','GHS02; extrem entzündbar');
INSERT INTO `list_gefahr` VALUES (000029,'Water-react. 2 or 3','H261','In Berührung mit Wasser entstehen entzündbare Gase.',0,0,0,'4.3','GHS02; extrem entzündbar');
INSERT INTO `list_gefahr` VALUES (000031,'Ox. Gas 1','H270','Oxidierende Gase',0,0,0,'2A','GHS 03; Oxidationsmittel');
INSERT INTO `list_gefahr` VALUES (000032,'Ox. Liq. 1','H271','Kann Brand oder Explosion verursachen; starkes Oxidationsmittel.',0,0,0,'5.1A','GHS 03; Oxidationsmittel');
INSERT INTO `list_gefahr` VALUES (000033,'Ox. Liq. 2  or 3','H272','Kann Brand verstärken; Oxidationsmittel.',0,0,0,'5.1B','GHS 03; Oxidationsmittel');
INSERT INTO `list_gefahr` VALUES (000035,'Ox. Sol. 1','H271','Kann Brand oder Explosion verursachen; starkes Oxidationsmittel.',0,0,0,'5.1A','GHS 03; Oxidationsmittel');
INSERT INTO `list_gefahr` VALUES (000036,'Ox. Sol. 2 or 3','H272','Kann Brand verstärken; Oxidationsmittel.',0,0,0,'5.1B','GHS 03; Oxidationsmittel');
INSERT INTO `list_gefahr` VALUES (000038,'Press. Gas ','H280','Enthält Gas unter Druck; kann bei Erwärmung explodieren.',0,0,0,'2A',NULL);
INSERT INTO `list_gefahr` VALUES (000039,'Press. Gas (refriger','H281','Enthält tiefgekühltes Gas; kann Kälteverbrennungen oder -Verletzungen verursachen.',0,0,0,'2A',NULL);
INSERT INTO `list_gefahr` VALUES (000042,'Met. Corr.1','H290','Kann gegenüber Metallen korrosiv sein.',0,0,0,'10-13','GHS05; Ätzend/Korrosiv');
INSERT INTO `list_gefahr` VALUES (000043,'Acute Tox.1 or 2 (oral)','H300','Lebensgefahr bei Verschlucken.',0,1,0,'6.1','GHS 06; Lebensgefahr');
INSERT INTO `list_gefahr` VALUES (000045,'Acute Tox.3 (oral)','H301','Giftig bei Verschlucken.',0,0,0,'6.1','GHS 06; Giftig');
INSERT INTO `list_gefahr` VALUES (000046,'Acute Tox. 4 (oral)','H302','Gesundheitsschädlich bei Verschlucken.',0,0,0,'10-13','GHS 07; Gesundheitsschädlich');
INSERT INTO `list_gefahr` VALUES (000047,'Asp. Tox. 1','H304','Kann bei Verschlucken und Eindringen in die Atemwege tödlich sein.',0,0,0,'10-13','GHS 08; Aspiration lebensgefährlich');
INSERT INTO `list_gefahr` VALUES (000048,'Acute Tox.1 or 2 (dermal)','H310','Lebensgefahr bei Hautkontakt.',0,1,0,'6.1','GHS 06; Lebensgefahr');
INSERT INTO `list_gefahr` VALUES (000050,'Acute Tox.3 (dermal)','H311','Giftig bei Hautkontakt.',0,0,0,'6.1','GHS 06; Giftig');
INSERT INTO `list_gefahr` VALUES (000051,'Acute Tox. 4 (dermal','H312','Gesundheitsschädlich bei Hautkontakt.',0,0,0,'10-13','GHS 07; Gesundheitsschädlich');
INSERT INTO `list_gefahr` VALUES (000052,'Skin. Corr. 1 (A, B or C)','H314','Verursacht schwere Verätzungen der Haut und schwere Augenschäden.',0,0,0,'8','GHS05; Ätzend/Korrosiv');
INSERT INTO `list_gefahr` VALUES (000055,'Skin Irrit. 2','H315','Verursacht Hautreizungen.',0,0,0,'10-13','GHS 07; Reizend');
INSERT INTO `list_gefahr` VALUES (000057,'Skin Sens. 1 (A or B)','H317','Kann allergische Hautreaktionen verursachen.',0,0,0,'10-13','GHS 07, Allergisierend bei Hautkontakt');
INSERT INTO `list_gefahr` VALUES (000059,'Eye Dam. 1','H318','Verursacht schwere Augenschäden.',0,0,0,'10-13','GHS05; Ätzend/Korrosiv');
INSERT INTO `list_gefahr` VALUES (000060,'Eye Irrit. 2','H319','Verursacht schwere Augenreizung.',0,0,0,'10-13','GHS 07; Reizend');
INSERT INTO `list_gefahr` VALUES (000061,'Acute Tox.1 or 2 (inhalativ)','H330','Lebensgefahr bei Einatmen.',0,1,0,'6.1','GHS 06; Lebensgefahr');
INSERT INTO `list_gefahr` VALUES (000063,'Acute Tox.3  (inhalative)','H331','Giftig bei Einatmen.',0,0,0,'6.1','GHS 06; Giftig');
INSERT INTO `list_gefahr` VALUES (000064,'Acute Tox. 4  (inhalative)','H332','Gesundheitsschädlich bei Einatmen.',0,0,0,'10-13','GHS 07; Gesundheitsschädlich');
INSERT INTO `list_gefahr` VALUES (000065,'Resp. Sens. 1 (A or B)','H334','Kann bei Einatmen Allergie, asthmaartige Symptome oder Atembeschwerden verursachen.',0,0,0,'10-13','GHS 08, Allergisierend bei Einatmen');
INSERT INTO `list_gefahr` VALUES (000068,'STOT SE 3; eye irr.','H335','Kann die Atemwege reizen.',0,0,0,'10-13','GHS 07; Reizend');
INSERT INTO `list_gefahr` VALUES (000069,'STOT SE 3; narcotic','H336','Kann Schläfrigkeit und Benommenheit verursachen.',0,0,0,'10-13','GHS 07; Betäubend');
INSERT INTO `list_gefahr` VALUES (000070,'Muta. 1 (A or B)','H340','Kann genetische Defekte verursachen.',1,1,0,'6.1','GHS 08; CMR-Stoff Kat. 1');
INSERT INTO `list_gefahr` VALUES (000072,'Muta. 2','H341','Kann vermutlich genetische Defekte verursachen.',1,1,0,'10-13','GHS 08; CMR-Stoff Kat. 2');
INSERT INTO `list_gefahr` VALUES (000073,'Carc. 1 (A or B)','H350','Kann Krebs erzeugen.',1,1,0,'6.1','GHS 08; CMR-Stoff Kat. 1');
INSERT INTO `list_gefahr` VALUES (000075,'Carc. 2','H351','Kann vermutlich Krebs erzeugen.',1,1,0,'10-13','GHS 08; CMR-Stoff Kat. 2');
INSERT INTO `list_gefahr` VALUES (000076,'Repr. 1 (A or B)','H360','Kann die Fruchtbarkeit beeinträchtigen und/oder das Kind im Mutterleib schädigen.',1,1,0,'6.1','GHS 08; CMR-Stoff Kat. 1');
INSERT INTO `list_gefahr` VALUES (000078,'Repr. 2','H361','Kann vermutlich die Fruchtbarkeit beeinträchtigen und/oder das Kind im Mutterleib schädigen.',1,1,0,'10-13','GHS 08; CMR-Stoff Kat. 2');
INSERT INTO `list_gefahr` VALUES (000079,'Lact.','H362','Kann Säuglinge über die Muttermilch schädigen.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000080,'STOT SE 1','H370','Schädigt die Organe.',0,0,0,'6.1','GHS 08; Schädigt die Organe.');
INSERT INTO `list_gefahr` VALUES (000081,'STOT SE 2','H371','Kann die Organe schädigen.',0,0,0,'10-13','GHS 08; Kann Organe schädigen.');
INSERT INTO `list_gefahr` VALUES (000082,'STOT RE 1','H372','Schädigt die Organe  bei längerer oder wiederholter Exposition.',0,0,0,'6.1','GHS 08; Schädigt die Organe.');
INSERT INTO `list_gefahr` VALUES (000083,'STOT RE 2','H373','Kann die Organe schädigen bei längerer oder wiederholter Exposition..',0,0,0,'10-13','GHS 08; Kann Organe schädigen.');
INSERT INTO `list_gefahr` VALUES (000084,'Aquatic Acute 1','H400','Sehr giftig für Wasserorganismen.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000085,'Aquatic Chronic 1','H410','Sehr giftig für Wasserorganismen mit langfristiger Wirkung.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000086,'Aquatic Chronic 2','H411','Giftig für Wasserorganismen, mit langfristiger Wirkung.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000087,'Aquatic Chronic 3','H412','Schädlich für Wasserorganismen, mit langfristiger Wirkung.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000088,'Aquatic Chronic 4','H413','Kann für Wasserorganismen schädlich sein, mit langfristiger Wirkung.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000089,'Ozone 1','H420','Schädigt die öffentliche Gesundheit und die Umwelt durch Ozonabbau in der äußeren Atmosphäre',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000091,'Acute Tox.3-4/ CMR 2','VGIII','Verpackungsgruppe III für Gefahrgut',1,1,0,'6.1','GHS 08; CMR-Stoff Kat. 2');
INSERT INTO `list_gefahr` VALUES (000092,'EUH 001','EUH 001','In trockenem Zustand explosionsgefährlich.',0,0,0,'10-13','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000093,'EUH 006','EUH 006','Mit und ohne Luft explosionsfähig.',0,1,0,'10-13','GHS01, explosiv');
INSERT INTO `list_gefahr` VALUES (000094,'EUH 014','EUH 014','Reagiert heftig mit Wasser.',0,0,0,'10-13','Reagiert heftig mit Wasser');
INSERT INTO `list_gefahr` VALUES (000095,'EUH 018','EUH 018','Kann bei Verwendung explosionsfähige/entzündbare Dampf/Luft-Gemische\r\nbilden.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000096,'EUH 019','EUH 019','Kann explosionsfähige Peroxide bilden.',0,0,0,'10-13','kann gefährlich altern');
INSERT INTO `list_gefahr` VALUES (000097,'EUH 044','EUH 044','Explosionsgefahr bei Erhitzen unter Einschluss.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000098,'EUH 029','EUH 029','Entwickelt bei Berührung mit Wasser giftige Gase.',0,0,0,'10-13','entwickelt giftige Gase mit Wasser/Säure');
INSERT INTO `list_gefahr` VALUES (000099,'EUH 031','EUH 031','Entwickelt bei Berührung mit Säure giftige Gase.',0,0,0,'10-13','entwickelt giftige Gase mit Wasser/Säure');
INSERT INTO `list_gefahr` VALUES (000100,'EUH 032','EUH 032','Entwickelt bei Berührung mit Säure sehr giftige Gase.',0,0,0,'10-13','entwickelt giftige Gase mit Wasser/Säure');
INSERT INTO `list_gefahr` VALUES (000101,'EUH 066','EUH 066','Wiederholter Kontakt kann zu spröder oder rissiger Haut führen.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000102,'EUH 070','EUH 070','Giftig bei Berührung mit den Augen.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000103,'EUH 071','EUH 071','Wirkt ätzend auf die Atemwege.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000104,'EUH 059','EUH 059','Die Ozonschicht schädigend.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000105,'EUH 201','EUH 201','Enthält Blei. Nicht für den Anstrich von Gegenständen verwenden, die von Kindern gekaut oder gelutscht werden könnten.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000106,'EUH 202','EUH 202','Cyanacrylat. Gefahr. Klebt innerhalb von Sekunden Haut und Augenlider zusammen.\r\nDarf nicht in die Hände von Kindern gelangen.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000107,'EUH 203','EUH 203','Enthält Chrom (VI). Kann allergische Reaktionen hervorrufen.',0,0,0,'10-13','GHS 07, Allergisierend bei Hautkontakt');
INSERT INTO `list_gefahr` VALUES (000108,'EUH 204','EUH 204','Enthält Isocyanate. Kann allergische Reaktionen hervorrufen.',0,0,0,'10-13','GHS 07, Allergisierend bei Hautkontakt');
INSERT INTO `list_gefahr` VALUES (000109,'EUH 205','EUH 205','Enthält epoxidhaltige Verbindungen. Kann allergische Reaktionen hervorrufen.',0,0,0,'10-13','GHS 07, Allergisierend bei Hautkontakt');
INSERT INTO `list_gefahr` VALUES (000110,'EUH 206','EUH 206','Achtung! Nicht zusammen mit anderen Produkten verwenden, da gefährliche\r\nGase (Chlor) freigesetzt werden können.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000111,'EUH 207','EUH 207','Achtung! Enthält Cadmium. Bei der Verwendung entstehen gefährliche Dämpfe.\r\nHinweise des Herstellers beachten. Sicherheitsanweisungen einhalten.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000112,'EUH 208','EUH 208','Enthält <Name des sensibilisierenden Stoffes>. Kann allergische Reaktionen hervorrufen.',0,0,0,'10-13','GHS 07, Allergisierend bei Hautkontakt');
INSERT INTO `list_gefahr` VALUES (000113,'EUH 209','EUH 209','Kann bei Verwendung leicht entzündbar werden.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000114,'EUH 210','EUH 210','Sicherheitsdatenblatt auf Anfrage erhältlich.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000115,'EUH 401','EUH 401','Zur Vermeidung von Risiken für Mensch und Umwelt die Gebrauchsanleitung einhalten.',0,0,0,'10-13',NULL);
INSERT INTO `list_gefahr` VALUES (000116,'Aerosol 3','H229','Behälter steht unter Druck: Kann bei Erwärmung bersten.',0,0,0,'2B',NULL);
INSERT INTO `list_gefahr` VALUES (000117,'kein gefährlicher Stoff gemäß ','nicht vorhanden','',0,0,0,'10-13',NULL);
/*!40000 ALTER TABLE `list_gefahr` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_katReiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_katReiter` (
  `autoID` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `kategorie` (`kategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_katReiter` WRITE;
/*!40000 ALTER TABLE `list_katReiter` DISABLE KEYS */;
INSERT INTO `list_katReiter` VALUES (1,'Ablage');
INSERT INTO `list_katReiter` VALUES (2,'Auswertung');
INSERT INTO `list_katReiter` VALUES (3,'Autorisierung');
INSERT INTO `list_katReiter` VALUES (4,'Haupttabelle');
INSERT INTO `list_katReiter` VALUES (5,'History');
INSERT INTO `list_katReiter` VALUES (6,'Liste');
INSERT INTO `list_katReiter` VALUES (7,'Programmierung');
INSERT INTO `list_katReiter` VALUES (8,'View');
INSERT INTO `list_katReiter` VALUES (9,'Zuordnung');
/*!40000 ALTER TABLE `list_katReiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_lgk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_lgk` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `lgk` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `priority` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `priority` (`priority`),
  UNIQUE KEY `lgk` (`lgk`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_lgk` WRITE;
/*!40000 ALTER TABLE `list_lgk` DISABLE KEYS */;
INSERT INTO `list_lgk` VALUES (000001,'1','Explosive Stoffe',5);
INSERT INTO `list_lgk` VALUES (000002,'2A','Verdichtete, verflüssigte oder unter Druck gelöste Gase',25);
INSERT INTO `list_lgk` VALUES (000003,'3','Entzündbare Flüssigkeiten ',70);
INSERT INTO `list_lgk` VALUES (000004,'4.1A','sonstige explosionsgefährliche Gefahrstoffe ',30);
INSERT INTO `list_lgk` VALUES (000005,'4.1B','entzündbare feste Gefahrstoffe',50);
INSERT INTO `list_lgk` VALUES (000006,'4.2','pyrophore oder selbsterhitzungsfähige Gefahrstoffe',40);
INSERT INTO `list_lgk` VALUES (000007,'4.3','Gefahrstoffe, die in Berührung mit Wasser entzündliche Gase entwickeln',45);
INSERT INTO `list_lgk` VALUES (000008,'5.1A','stark oxidierende Gefahrstoffe',60);
INSERT INTO `list_lgk` VALUES (000009,'5.1B','oxidierende Gefahrstoffe',65);
INSERT INTO `list_lgk` VALUES (000010,'5.1C','Ammoniumnitrat und ammoniumnitrathaltige Zubereitungen',55);
INSERT INTO `list_lgk` VALUES (000011,'5.2','Organische Peroxide und selbstzersetzliche Gefahrstoffe',35);
INSERT INTO `list_lgk` VALUES (000014,'6.2','Ansteckungsgefährliche Stoffe',10);
INSERT INTO `list_lgk` VALUES (000015,'7','Radioaktive Stoffe',15);
INSERT INTO `list_lgk` VALUES (000019,'10','Brennbare Flüssigkeiten soweit keiner zuvor genannten LGK ',117);
INSERT INTO `list_lgk` VALUES (000020,'11','Brennbare Feststoffe, die keine zuvor genannten LGK zugeordnet ist',118);
INSERT INTO `list_lgk` VALUES (000021,'12','Nicht brennbare Flüssigkeiten, die keine zuvor genannten LGK zugeordnet ist',119);
INSERT INTO `list_lgk` VALUES (000022,'13','Nicht brennbare Feststoffe,die keine zuvor genannten LGK zugeordnet ist',120);
INSERT INTO `list_lgk` VALUES (000023,'10-13','Gefahrstoffe, die keine zuvor genannten LGK zugeordnet sind',85);
INSERT INTO `list_lgk` VALUES (000027,'--','fake fuer Datenuebernahme',255);
INSERT INTO `list_lgk` VALUES (000029,'2B','Aerosolpackungen',20);
INSERT INTO `list_lgk` VALUES (000030,'6.1','akut toxische oder chronisch wirkende Gefahrstoffe',75);
INSERT INTO `list_lgk` VALUES (000032,'8','ätzende Gefahrstoffe',80);
/*!40000 ALTER TABLE `list_lgk` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_reiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_reiter` (
  `autoID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `reiter` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `kategorie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `favorit` tinyint(1) NOT NULL DEFAULT '0',
  `history` tinyint(1) NOT NULL DEFAULT '0',
  `bedeutung` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`autoID`),
  UNIQUE KEY `reiter` (`reiter`),
  KEY `kategorie` (`kategorie`),
  CONSTRAINT `list_reiter_kateg` FOREIGN KEY (`kategorie`) REFERENCES `list_katReiter` (`kategorie`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Reiterlinks fuer Xataface';
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_reiter` WRITE;
/*!40000 ALTER TABLE `list_reiter` DISABLE KEYS */;
INSERT INTO `list_reiter` VALUES (000001,'list_gefahr','Haupttabelle',1,1,'Globale Liste Gefahrenmerkmale fuer Auswahl in Chemie-DB\'s');
INSERT INTO `list_reiter` VALUES (000002,'list_lgk','Haupttabelle',1,1,'Globale Liste Lagerklassen fuer Auswahl in Chemie-DB\'s');
INSERT INTO `list_reiter` VALUES (000003,'list_katReiter','Liste',0,0,'Zugehoerigkeit DB-Tabellen');
INSERT INTO `list_reiter` VALUES (000004,'list_reiter','Liste',0,0,'Sammelcontainer fuer Tabbutton \"mehr ..\"');
INSERT INTO `list_reiter` VALUES (000006,'view_user','View',0,0,'Auswahlliste fuer aktive und nicht abgelaufene Benutzer');
INSERT INTO `list_reiter` VALUES (000007,'view_favorit','Programmierung',0,0,'Für den schnelleren Zugriff unter dem Menüpunkt \'Favorit');
INSERT INTO `list_reiter` VALUES (000008,'view_reiter','Programmierung',0,0,'Hole alle Tabellen von Datenbank von mysql');
INSERT INTO `list_reiter` VALUES (000009,'view_gefahr','View',0,0,'Zeige von allen Chemie-DB\'s die als gefaehrlich deklarierten Chemiestoffe');
INSERT INTO `list_reiter` VALUES (000010,'view_chemAll','View',0,0,'Zeige alle Chemiestoffe von allen Chemie-DB\'s');
INSERT INTO `list_reiter` VALUES (000011,'view_ablageAll','View',1,0,'Betriebsanw. und Sicherh.-Datenbl. andere Gruppen');
INSERT INTO `list_reiter` VALUES (000012,'sys_user','Autorisierung',1,1,'Autorisierung und Berechtigung Benutzer');
INSERT INTO `list_reiter` VALUES (000013,'list_role','Autorisierung',1,0,'Liste aller Berechtigungen (Rollen)');
/*!40000 ALTER TABLE `list_reiter` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `list_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_role` (
  `rolID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rolID`),
  UNIQUE KEY `role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `list_role` WRITE;
/*!40000 ALTER TABLE `list_role` DISABLE KEYS */;
INSERT INTO `list_role` VALUES (000001,'NO ACCESS','No_Access');
INSERT INTO `list_role` VALUES (000002,'READ ONLY','view, list, calendar, view xml, show all, find, navigate');
INSERT INTO `list_role` VALUES (000003,'EDIT','READ_ONLY and edit, new record, remove, import, translate, copy');
INSERT INTO `list_role` VALUES (000004,'DELETE','EDIT and delete and delete found');
INSERT INTO `list_role` VALUES (000005,'OWNER','DELETE except navigate, new, and delete found');
INSERT INTO `list_role` VALUES (000006,'REVIEWER','READ_ONLY and edit and translate');
INSERT INTO `list_role` VALUES (000007,'USER','READ_ONLY and add new related record');
INSERT INTO `list_role` VALUES (000008,'ADMIN','DELETE and xml_view');
INSERT INTO `list_role` VALUES (000009,'MANAGER','ADMIN and manage, manage_migrate, manage_build_index, and install');
/*!40000 ALTER TABLE `list_role` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_user` (
  `logID` smallint(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `login` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bearbeiter` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zeitstempel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`logID`) USING BTREE,
  UNIQUE KEY `login` (`login`) USING BTREE,
  UNIQUE KEY `email` (`email`),
  KEY `role` (`role`),
  CONSTRAINT `sysUser_listRole` FOREIGN KEY (`role`) REFERENCES `list_role` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (000001,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','MANAGER','','import','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `view_ablageAll`;
/*!50001 DROP VIEW IF EXISTS `view_ablageAll`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_ablageAll` (
  `ablageID` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `bezeichnung` tinyint NOT NULL,
  `filename` tinyint NOT NULL,
  `gruppe` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_chemAll`;
/*!50001 DROP VIEW IF EXISTS `view_chemAll`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_chemAll` (
  `tabID` tinyint NOT NULL,
  `substanz` tinyint NOT NULL,
  `reinheit` tinyint NOT NULL,
  `lgk` tinyint NOT NULL,
  `cas` tinyint NOT NULL,
  `einheit` tinyint NOT NULL,
  `bFileID` tinyint NOT NULL,
  `bFilename` tinyint NOT NULL,
  `sFileID` tinyint NOT NULL,
  `sFilename` tinyint NOT NULL,
  `gruppe` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_favorit`;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_favorit` (
  `autoID` tinyint NOT NULL,
  `reiter` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `favorit` tinyint NOT NULL,
  `bedeutung` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_gefahr`;
/*!50001 DROP VIEW IF EXISTS `view_gefahr`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_gefahr` (
  `autoID` tinyint NOT NULL,
  `tabID` tinyint NOT NULL,
  `substanz` tinyint NOT NULL,
  `kategorie` tinyint NOT NULL,
  `gruppe` tinyint NOT NULL,
  `bearbeiter` tinyint NOT NULL,
  `zeitstempel` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_reiter`;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_reiter` (
  `reiter` tinyint NOT NULL,
  `table_type` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;
DROP TABLE IF EXISTS `view_user`;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_user` (
  `userID` tinyint NOT NULL,
  `login` tinyint NOT NULL,
  `sort` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

USE `mpidb_mpg_gfk`;
/*!50001 DROP TABLE IF EXISTS `view_ablageAll`*/;
/*!50001 DROP VIEW IF EXISTS `view_ablageAll`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_ablageAll` AS select '000001' AS `ablageID`,'Betriebsanweisung' AS `kategorie`,'Fake Eintrag wenn keine chemDB' AS `bezeichnung`,'noName' AS `filename`,'mpg' AS `gruppe`,'initial' AS `bearbeiter`,'2016-04-22 11:34:18' AS `zeitstempel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_chemAll`*/;
/*!50001 DROP VIEW IF EXISTS `view_chemAll`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_chemAll` AS select '1' AS `tabID`,'Fake Eintrag wenn keine chemDB' AS `substanz`,'-' AS `reinheit`,'-' AS `lgk`,'-' AS `cas`,'-' AS `einheit`,'0' AS `bFileID`,'-' AS `bFilename`,'0' AS `sFileID`,'-' AS `sFilename`,'mpg' AS `gruppe`,'initial' AS `bearbeiter`,'2016-05-30 11:34:18' AS `zeitstempel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_favorit`*/;
/*!50001 DROP VIEW IF EXISTS `view_favorit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_favorit` AS select `list_reiter`.`autoID` AS `autoID`,`list_reiter`.`reiter` AS `reiter`,`list_reiter`.`kategorie` AS `kategorie`,`list_reiter`.`favorit` AS `favorit`,`list_reiter`.`bedeutung` AS `bedeutung` from `list_reiter` where (`list_reiter`.`favorit` = '1') order by `list_reiter`.`reiter` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_gefahr`*/;
/*!50001 DROP VIEW IF EXISTS `view_gefahr`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_gefahr` AS select '1' AS `autoID`,'1' AS `tabID`,'Fake Eintrag wenn keine chemDB' AS `substanz`,'-' AS `kategorie`,'mpg' AS `gruppe`,'initial' AS `bearbeiter`,'2016-03-02 11:34:18' AS `zeitstempel` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_reiter`*/;
/*!50001 DROP VIEW IF EXISTS `view_reiter`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_reiter` AS select (convert(`information_schema`.`tables`.`TABLE_NAME` using utf8) collate utf8_unicode_ci) AS `reiter`,`information_schema`.`tables`.`TABLE_TYPE` AS `table_type` from `information_schema`.`tables` where ((`information_schema`.`tables`.`TABLE_SCHEMA` = (select database())) and ((`information_schema`.`tables`.`TABLE_TYPE` = 'base table') or (`information_schema`.`tables`.`TABLE_TYPE` = 'view'))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!50001 DROP TABLE IF EXISTS `view_user`*/;
/*!50001 DROP VIEW IF EXISTS `view_user`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_user` AS select '000001' AS `userID`,'mpg_local' AS `login`,'MPG, Version (mpg_local)' AS `sort` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

